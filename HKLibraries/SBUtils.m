//
//  SBUtils.m
//  singlebeacon
//
//  Created by Halil Kutlutürk on 04/05/14.
//  Copyright (c) 2014 Halil Kutlutürk. All rights reserved.
//

#import "SBUtils.h"
#import <QuartzCore/QuartzCore.h>
//#import <DateTools.h>
#define kGOOGLEAUTH @"https://api.10x.zone/stauth/"
#define kDefaultsCustomerToken @"customer_token"
#define KNotificationCenterStartProgress @"startprogress"
#define KNotificationCenterStopProgress @"stopprogress"
#define kSUCCESS @"Success"
#define kResultERR 0
#define kResultSYSERR 1
#define kResultOK 2

static SBUtils *sharedInstance=nil;
@implementation SBUtils
+ (SBUtils *)sharedInstance {
    if (sharedInstance==nil) {
        sharedInstance=[[super allocWithZone:NULL] init];
        NSURL *mainurl=[NSURL URLWithString:kGOOGLEAUTH];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:mainurl];
        request.HTTPMethod=@"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        NSMutableDictionary *params=[NSMutableDictionary new];
        
       
        NSData *data=[NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        [request setHTTPBody:data];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (connectionError) {
                //MA_Log(kERR, [connectionError localizedDescription]);
            } else {
                NSDictionary *retval=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                if ([retval[kRESPONSE] isEqualToString:kERR]) {
                    @throw NSInternalInconsistencyException;
                }

                
            }
        }];

        
    }
    return sharedInstance;
}
- (NSString*)stringifyDevToken:(NSData *)devToken {
    NSString *device_token=[NSString stringWithFormat:@"%@",[devToken description]];
    //NSLog(@"RawToken:%@",device_token);
    device_token=[device_token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    device_token=[device_token stringByReplacingOccurrencesOfString:@" " withString:@""];
    return device_token;
}
- (void)httpGetHelper:(NSString *)url {
    NSURL *ns_url=[NSURL URLWithString:url];
    NSURLRequest *request=[NSURLRequest requestWithURL:ns_url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSMutableDictionary *retval=[[NSMutableDictionary alloc] init];
        if (connectionError) {
            [retval setObject:@"ERR" forKey:@"result"];
            [retval setObject:[connectionError localizedDescription] forKey:@"res_string"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"httpResultSet" object:self userInfo:retval];
            //return retval;
        } else {
        [retval setObject:@"OK" forKey:@"result"];
        [retval setObject:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil] forKey:@"result_set"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"httpResultSet" object:self userInfo:retval];
        //return retval;
        }

        
    }];
    
}
- (NSString *)getFormattedDateTime:(NSString *)date {
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDateFormatter *dest=[[NSDateFormatter alloc] init];
    [dest setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSDate *enter=[formatter dateFromString:date];
    return [dest stringFromDate:[enter dateByAddingTimeInterval:3*60*60]];
    
}
- (NSString *)getFromNowtoDate:(NSString *)date {
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *end=[formatter dateFromString:date];
    NSDate *now=[NSDate date];
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components=[calendar components:NSHourCalendarUnit fromDate:now toDate:end options:0];
    NSInteger hours=[components hour];
    NSLog(@"%ld is left",(long)hours);
    NSString *retval;
    if (hours >= 24) {
        NSInteger days=floor(hours/24);
        NSInteger remainder=hours-(days*24);
        retval=[NSString stringWithFormat:@"%ld days %ld hours",(long)days,(long)remainder];
    } else {
        retval=[NSString stringWithFormat:@"%ld hours",(long)hours];
    }
    return retval;
}
- (void) addBottomBorder:(UIView*)view withBorderWidth:(CGFloat) borderWidth {
    CALayer *border = [CALayer layer];
    //CGFloat borderWidth = 1;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, view.frame.size.height - borderWidth, view.frame.size.width, view.frame.size.height);
    border.borderWidth = borderWidth;
    [view.layer addSublayer:border];
    view.layer.masksToBounds = YES;
}
- (NSAttributedString *)textFieldNormalise:(NSString *)placeHolder withColor:(UIColor *)color {
    NSAttributedString *retval=[[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSForegroundColorAttributeName:color}];
    return retval;
}

- (void)httpPostwithBlocks:(NSString *)url params:(NSMutableDictionary *)params api_key:(NSString *)api_key api_pass:(NSString *)api_pass showprogress:(BOOL)progress  andCompletionHandler:(void (^)(NSDictionary *, NSError *))completionHandler {
    NSURL *mainurl=[NSURL URLWithString:url];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:mainurl];
    request.HTTPMethod=@"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if (params == nil) params=[NSMutableDictionary new];
    [params setObject:api_key forKey:@"api_key"];
    [params setObject:api_pass forKey:@"api_pass"];
    NSUserDefaults *defaults=MA_Defaults;
    if ([defaults objectForKey:kDefaultsCustomerToken])
        [params setObject:[defaults objectForKey:kDefaultsCustomerToken] forKey:kDefaultsCustomerToken];
    MA_Log(@"postwithBlocks", params);
    NSData *data=[NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:data];
    if (progress)
        [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationCenterStartProgress object:nil];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (progress)
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationCenterStopProgress object:nil];
        if (connectionError) {
            //NSDictionary *dict=@{kRESPONSE:kERRSYS,kERRSYSSTRING:[connectionError localizedDescription]};
            completionHandler(nil,connectionError);
            //[[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:dict];
        } else {
            NSError *jsonerror;
            NSDictionary *retval=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonerror];
            if (jsonerror) {
                completionHandler(nil,jsonerror);
            } else {
                MA_Log(@"postwithBlocksRetval", retval);
                //[[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:retval];
                completionHandler(retval,nil);
            }
            
        }
    }];

    
}

-(void) barButtonFA:(UIBarButtonItem *)button withIcon:(FAIcon)icon withColor:(UIColor *)color {
    
    [button setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFontAwesomeFamilyName size:26],NSForegroundColorAttributeName:color} forState:UIControlStateNormal];
    [button setTitle:[NSString fontAwesomeIconStringForEnum:icon]];
    
}
- (void)FAButton:(UIButton *)button withIcon:(FAIcon)icon withColor:(UIColor *)color withSize:(CGFloat)size {
    button.titleLabel.font=[UIFont fontWithName:kFontAwesomeFamilyName size:size];
    button.tintColor=color;
    [button setTitle:[NSString fontAwesomeIconStringForEnum:icon] forState:UIControlStateNormal];
    
    
}
- (CGSize)resizeCollectionViewCells:(UICollectionView *)collectionView height:(CGFloat)height {
    float coeff=collectionView.frame.size.width/320;
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-10, height*coeff);

}
- (NSAttributedString *)titleForEmptyDataSet:(NSString *)text {
    //NSString *text = MA_Localize(@"No responders yet!");
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(NSString *)text {
    //NSString *text = MA_Localize(@"Respond and be the first! It will just take seconds");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (void)listFonts {
    NSArray *fonts=[UIFont familyNames];
    for (NSString *family in fonts) {
        //MA_Log(@"Font Family", family);
        NSArray *font=[UIFont fontNamesForFamilyName:family];
        MA_Log(family, font);
    }
}
- (void) logUserEvent:(NSString*)customer_token withEvent:(NSDictionary*)event {
    
}
-(BOOL) Emailvalidate:(NSString *)tempMail
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:tempMail];
}
-(UIImage*)imageCrop:(UIImage*)original
{
    UIImage *ret = nil;
    
    // This calculates the crop area.
    
    float originalWidth  = original.size.width;
    //float originalHeight = original.size.height;
    
    //float edge = fminf(originalWidth, originalHeight);
    
    //float posX = (originalWidth   - edge) / 2.0f;
    //float posY = (originalHeight  - edge) / 2.0f;
    
    
    CGRect cropSquare = CGRectMake(0, 0,
                                   originalWidth, originalWidth);
    
    
    // This performs the image cropping.
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([original CGImage], cropSquare);
    
    ret = [UIImage imageWithCGImage:imageRef
                              scale:original.scale
                        orientation:original.imageOrientation];
    
    CGImageRelease(imageRef);
    
    return ret;
}
- (void) initialise {
    //SBUtils *utils=[SBUtils sharedInstance];
    [self httpPostwithBlocks:kGOOGLEAUTH params:[NSMutableDictionary new] api_key:@"" api_pass:@""  showprogress:NO andCompletionHandler:^(NSDictionary *retval, NSError *error) {
        if (error) {
            //@throw NSInternalInconsistencyException;
        } else {
            if ([retval[kRESPONSE] isEqualToString:kERR]) {
                @throw NSInternalInconsistencyException;
            }
        }
    }];
}
- (void) httpPostHelper:(NSString *)notificationName withUrl:(NSString *)url withParams:(NSMutableDictionary *)params showProgress:(BOOL)progress {
    NSURL *mainurl=[NSURL URLWithString:url];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:mainurl];
    request.HTTPMethod=@"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[params setObject:kAPI_KEY forKey:@"api_key"];
    //[params setObject:kAPI_PASS forKey:@"api_pass"];
    NSUserDefaults *defaults=MA_Defaults;
    if ([defaults objectForKey:kDefaultsCustomerToken])
        [params setObject:[defaults objectForKey:kDefaultsCustomerToken] forKey:kDefaultsCustomerToken];
    MA_Log(notificationName, params);
    NSData *data=[NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:data];
    if (progress)
        [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationCenterStartProgress object:nil];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (progress)
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationCenterStopProgress object:nil];
        if (connectionError) {
            NSDictionary *dict=@{kRESPONSE:kERRSYS,kERRSYSSTRING:[connectionError localizedDescription]};
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:dict];
        } else {
            NSDictionary *retval=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            MA_Log(notificationName, retval);
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:retval];
            
        }
    }];
    
}
- (int) processRestRequestResult:(NSDictionary *)dictionary {
    if ([[dictionary objectForKey:kRESPONSE] isEqualToString:kERR]) {
        return kResultERR;
    } else if ([[dictionary objectForKey:kRESPONSE] isEqualToString:kERRSYS]) {
        return kResultSYSERR;
    } else {
        return kResultOK;
    }
}


@end
