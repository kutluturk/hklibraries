//
//  TDImagePickerViewController.h
//  rollo
//
//  Created by Halil Kutlutürk on 10/04/15.
//  Copyright (c) 2015 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDImagePickerViewController : UIImagePickerController

@end
