//
//  TDTapGestureRecognizer.h
//  rollo
//
//  Created by Halil Kutlutürk on 13/04/15.
//  Copyright (c) 2015 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDTapGestureRecognizer : UITapGestureRecognizer
@property (nonatomic, readwrite, retain) id indexPath;
@property (nonatomic, readwrite, retain) id userData;
@end
