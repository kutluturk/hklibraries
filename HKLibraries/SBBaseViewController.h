//
//  SBBaseViewController.h
//  
//
//  Created by Halil Kutlutürk on 04/05/14.
//  Copyright (c) 2014 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CRToast.h>
//#import <MRProgress.h>
//#import "SBUtils.h"

//#import <AMSmoothAlertView.h>
//#import <RESideMenu.h>
#import <SCLAlertView.h>
#import <MBProgressHUD.h>
#import <IQKeyboardManager.h>
#import "UIAlertController+Rotation.h"
//#import <TDFramework/TDFramework.h>
#import "rollo.h"
#import "HKLibraries/HKLibraries.h"

@interface SBBaseViewController : UIViewController <MBProgressHUDDelegate>

- (void) closeProgressViews;
- (void) alertError:(NSString*)text title:(NSString*)title;
- (void) alertInfo:(NSString*)text title:(NSString*)title;
- (void) alertSuccess:(NSString*)text title:(NSString*)title;
- (void) showProgress:(NSString*)title;

@end

