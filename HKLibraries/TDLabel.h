//
//  TDLabel.h
//  rollo
//
//  Created by Halil Kutlutürk on 15/04/15.
//  Copyright (c) 2015 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDLabel : UILabel
@property (nonatomic, readwrite, retain) id userData;
@end
