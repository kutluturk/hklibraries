//
//  SBUtils.h
//  singlebeacon
//
//  Created by Halil Kutlutürk on 04/05/14.
//  Copyright (c) 2014 Halil Kutlutürk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSString+FontAwesome.h"

// SYSTEM CONSTANTS





// GENERAL CONSTANTS

#define kOK @"OK"
#define kERR @"ERR"
#define kRESPONSE @"response"
#define kERRSYS @"ERROR"
#define kERRSYSSTRING @"kerrstr"
#define kVALUES @"values"



// MACRO DEFINITIONS
#define MA_Localize(t) NSLocalizedString(t, nil)
#define MA_Log(t,y) NSLog(@"%@:%@",t,y)
#define MA_Defaults [NSUserDefaults standardUserDefaults]

// ERROR STRINGS
#define kErrorSYSERROR @"Oops! System Error. Please try again later"
#define kError @"ERROR"
//#define kAPI_KEY @"NHWE9RDANJEWF4"
//#define kAPI_PASS @"9023EIDER1289D"

// INTERFACE
@interface SBUtils : NSObject
+ (SBUtils*)sharedInstance;
- (NSString*)stringifyDevToken:(NSData*)devToken;
- (NSString*)getFormattedDateTime:(NSString*)date;
- (void) addBottomBorder:(UIView*)view withBorderWidth:(CGFloat) borderWidth;
- (NSAttributedString*) textFieldNormalise:(NSString*)placeHolder withColor:(UIColor*) color;
- (void) logUserEvent:(NSString*)customer_token withEvent:(NSDictionary*)event;
- (void) httpPostHelper:(NSString*)notificationName  withUrl:(NSString*)url withParams:(NSMutableDictionary*)params showProgress:(BOOL)progress;
- (void) barButtonFA:(UIBarButtonItem*)button withIcon:(FAIcon)icon withColor:(UIColor*)color;
- (NSString *)getFromNowtoDate:(NSString*)date;
- (void) FAButton:(UIButton*)button withIcon:(FAIcon)icon withColor:(UIColor*)color withSize:(CGFloat)size;
- (CGSize) resizeCollectionViewCells:(UICollectionView*)collectionView height:(CGFloat)height;
- (NSAttributedString *)titleForEmptyDataSet:(NSString *)text;
- (NSAttributedString *)descriptionForEmptyDataSet:(NSString *)text;
- (void) listFonts;
- (void) httpPostwithBlocks:(NSString*)url params:(NSMutableDictionary*)params api_key:(NSString*)api_key api_pass:(NSString*)api_pass showprogress:(BOOL)progress andCompletionHandler:(void (^)(NSDictionary* retval,NSError *error))completionHandler;
-(BOOL) Emailvalidate:(NSString *)tempMail;
-(UIImage*)imageCrop:(UIImage*)original;
- (void) initialise;
- (int) processRestRequestResult:(NSDictionary*)dictionary;

@end

