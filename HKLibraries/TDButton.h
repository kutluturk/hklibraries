//
//  TDButton.h
//  10xzone
//
//  Created by Halil Kutlutürk on 22/06/14.
//  Copyright (c) 2014 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDButton : UIButton
@property (nonatomic, readwrite, retain) id userData;
@end
