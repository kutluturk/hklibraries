//
//  SBBaseViewController.m
//  singlebeacon
//
//  Created by Halil Kutlutürk on 04/05/14.
//  Copyright (c) 2014 Halil Kutlutürk. All rights reserved.
//

#import "SBBaseViewController.h"
//#import "TDAppDelegate.h"
//#import "TDSettingsViewController.h"
//#import "TDAlertController.h"

@interface SBBaseViewController ()

@end

@implementation SBBaseViewController {
    //SBUtils *utils;
    MBProgressHUD *hud;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedRemoteNotification:) name:kNotificationCenterDefaultNotificationListener object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startProgress) name:KNotificationCenterStartProgress object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeProgressViews) name:KNotificationCenterStopProgress object:nil];
    //defaults=[NSUserDefaults standardUserDefaults];
    //utils=[SBUtils sharedInstance];
    
    // Do any additional setup after loading the view.
}
- (void) startProgress {
    hud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view bringSubviewToFront:hud];
    //[hud hide:YES];
    hud.delegate=self;
    hud.labelText=MA_Localize(@"Please wait...");
    hud.mode=MBProgressHUDModeIndeterminate;

}
-(void) receivedRemoteNotification:(NSNotification*)userInfo {
    //NSLog(@"%@",userInfo);
    //NSDictionary *dict=userInfo.userInfo;
    //NSDictionary *aps=[dict objectForKey:@"aps"];
    //[self showToast:[aps objectForKey:@"alert"] withColor:[UIColor greenColor]];
    //UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:[aps objectForKey:@"title"] delegate:nil cancelButtonTitle:@"Kapat" otherButtonTitles:nil, nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void) closeProgressViews {
    //[MRProgressOverlayView dismissAllOverlaysForView:self.view animated:YES];
    //[SCLAlertView ]
    [hud removeFromSuperview];
    hud.delegate=nil;
    hud=nil;
}
#pragma end

#pragma mark CRToast

- (void)alertError:(NSString *)text title:(NSString*)title {
    if (title==nil) title=MA_Localize(@"Error");
    /*AMSmoothAlertView *alert=[[AMSmoothAlertView alloc]initDropAlertWithTitle:title andText:text andCancelButton:NO forAlertType:AlertFailure];
    [alert setTitleFont:[utils getFont:EXTRABOLD withSize:18]];
    [alert setTextFont:[utils getFont:LIGHT withSize:12]];
    [alert show];*/
    //SCLAlertView *alert=[[SCLAlertView alloc] init];
    //[alert showError:self title:title subTitle:text closeButtonTitle:MA_Localize(@"Done") duration:15.0f];
    
    //UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
    //[alert show];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        TDAlertController *alert=[TDAlertController alertControllerWithTitle:@"Oops!" message:text  preferredStyle:UIAlertControllerStyleAlert];
        
        
        //UIAlertAction *cancel=[UIAlertAction actionWithTitle:MA_Localize(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:MA_Localize(@"OK") style:UIAlertActionStyleDefault handler:nil];
    
            
    
        //[alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Oops!" message:text delegate:self cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
        [alert show];
    }

}
- (void)alertInfo:(NSString *)text title:(NSString*)title {
    if (title==nil) title=MA_Localize(@"Info");
    /*AMSmoothAlertView *alert=[[AMSmoothAlertView alloc]initDropAlertWithTitle:title andText:text andCancelButton:NO forAlertType:AlertInfo];
    [alert setTitleFont:[utils getFont:EXTRABOLD withSize:18]];
    [alert setTextFont:[utils getFont:LIGHT withSize:12]];
    [alert show];*/
    //SCLAlertView *alert=[[SCLAlertView alloc] init];
    //[alert showInfo:self title:title subTitle:text closeButtonTitle:MA_Localize(@"Done") duration:15.0f];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        TDAlertController *alert=[TDAlertController alertControllerWithTitle:title message:text  preferredStyle:UIAlertControllerStyleAlert];
        
        
        //UIAlertAction *cancel=[UIAlertAction actionWithTitle:MA_Localize(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:MA_Localize(@"OK") style:UIAlertActionStyleDefault handler:nil];
        
        
        
        //[alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
        [alert show];
    }

    //UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
    //[alert show];

}
- (void) alertSuccess:(NSString*)text title:(NSString*)title {
    if (title==nil) title=MA_Localize(@"Success");
    /*AMSmoothAlertView *alert=[[AMSmoothAlertView alloc]initDropAlertWithTitle:title andText:text andCancelButton:NO forAlertType:AlertSuccess];
    [alert setTitleFont:[utils getFont:EXTRABOLD withSize:18]];
    [alert setTextFont:[utils getFont:LIGHT withSize:12]];
    [alert show];*/
    //SCLAlertView *alert=[[SCLAlertView alloc] init];
    //[alert showSuccess:self title:title subTitle:text closeButtonTitle:MA_Localize(@"Done") duration:15.0f];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        TDAlertController *alert=[TDAlertController alertControllerWithTitle:title message:text  preferredStyle:UIAlertControllerStyleAlert];
        
        
        //UIAlertAction *cancel=[UIAlertAction actionWithTitle:MA_Localize(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
        
        UIAlertAction *ok=[UIAlertAction actionWithTitle:MA_Localize(@"OK") style:UIAlertActionStyleDefault handler:nil];
        
        
        
        //[alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
        [alert show];
    }

    //UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:MA_Localize(@"OK") otherButtonTitles:nil, nil];
    //[alert show];
}

- (void) showProgress:(NSString *)title {
    hud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view bringSubviewToFront:hud];
    //[hud hide:YES];
    hud.delegate=self;
    hud.labelText=MA_Localize(@"Please wait...");
    hud.mode=MBProgressHUDModeIndeterminate;

}
- (BOOL)shouldAutorotate {
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;

}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
