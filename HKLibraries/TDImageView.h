//
//  TDImageView.h
//  10xzone
//
//  Created by Halil Kutluturk on 24/01/15.
//  Copyright (c) 2015 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDImageView : UIImageView
@property (nonatomic, readwrite, retain) id userData;
@end
