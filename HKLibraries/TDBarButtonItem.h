//
//  TDBarButtonItem.h
//  10xzone
//
//  Created by Halil Kutluturk on 25/01/15.
//  Copyright (c) 2015 Halil Kutlutürk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDBarButtonItem : UIBarButtonItem
@property (nonatomic, readwrite, retain) id userData;
@end
