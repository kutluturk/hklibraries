# README #

For the eyes of the gigster reviewer.



### What is this repository for? ###

HKLibraries is a Xcode fat library (arm7,arm7s,i368,arm64) which can be imported into IOS development projects. 

SBUtils is the util singleton class and out of the box, it provides helper functions for common development tasks like email validation, Restful post helper. 

Any other function can be added and used in many other projects at the same time.

### How do I get set up? ###

Just download and compile the universal library. Import .a file to the main project.
